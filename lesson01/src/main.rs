extern crate rand;

use rand::Rng;
use std::cmp::Ordering;
use std::io;

// https://doc.rust-lang.org/book/2018-edition/ch02-00-guessing-game-tutorial.html

fn main() {
    println!("Guess the number");

    let random_number = rand::thread_rng().gen_range(1, 101);

    loop {
        let mut value = String::new();
        io::stdin()
            .read_line(&mut value)
            .expect("Unable to read the line");

        let guess: i32 = match value.trim().parse() {
            Ok(num) => num,
            Err(_) => continue
        };

        match guess.cmp(&random_number) {
            Ordering::Less => println!("Too small!"),
            Ordering::Greater => println!("Too big!"),
            Ordering::Equal => {
                println!("You have won!");
                break;
            }
        };
    }

}
